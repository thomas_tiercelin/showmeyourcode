package com.showmeyourcode;

import static com.googlecode.objectify.ObjectifyService.ofy;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
@Cache
@Entity
public class Post {
	@Id private Long id;
	@Index private Date createdAt = new Date();
	@Index private Date updatedAt = new Date();
	@Index private Date publishedAt = new Date();
	
	@Index private String path;
	private String title;
	private String description;
	private String youtubeId;
	private String content;
	
	public static Post get(Long id) {
		return ofy().load().type(Post.class).id(id).now();
	}
	
	public void save() {
		this.setUpdatedAt(new Date());
		ofy().save().entity(this).now();
	}
	public static List<Post> listAll() {
		return ofy().load().type(Post.class).list();
	}
	public void remove() {
		ofy().delete().entity(this).now();
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public Date getCreatedAt() {
		return this.createdAt;
	}
	
	public Date getUpdatedAt() {
		return this.updatedAt;
	}
	
	public Date getPublishedAt() {
		return this.publishedAt;
	}	
	
	public void setCreatedAt(Date date) {
		this.createdAt = date;
	}
	
	public void setUpdatedAt(Date date) {
		this.updatedAt = date;
	}	
	
	public void setPublishedAt(Date date) {
		this.publishedAt = date;
	}		

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getYoutubeId() {
		return youtubeId;
	}

	public void setYoutubeId(String youtubeId) {
		this.youtubeId = youtubeId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	public String getPath() {
		return this.path;
	}
	
	public void setPath(String path) {
		this.path = path;
	}
	
	public static Post findByPath(String path) {
		return ofy().load().type(Post.class).filter("path =", path).first().now();
	}
	
	public static Post getForServletRequest(HttpServletRequest req) {
		System.out.println(req.getRequestURI());
		Post post = Post.findByPath(req.getRequestURI());
		System.out.println(""+post);
		return post;
	}
}
