package com.showmeyourcode;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiMethod.HttpMethod;
import com.google.api.server.spi.config.Named;
import com.google.api.server.spi.response.CollectionResponse;
import com.googlecode.objectify.ObjectifyService;

@Api(name="smyc",version="v1")
public class PostEndpoint {
	
	static {
		ObjectifyService.register(Post.class);
	}
	
	@ApiMethod(name = "post.list", path="post",httpMethod = HttpMethod.GET)
	public CollectionResponse<Post> listPosts() {				
		return CollectionResponse.<Post> builder().setItems(Post.listAll()).build();
	}
	
	@ApiMethod(name = "post.get", path="post/{postId}",httpMethod = HttpMethod.GET)
	public Post getPost(
			@Named("postId") Long postId
			) {
		
		return Post.get(postId);
		
	}	
	
	@ApiMethod(name = "post.remove", path="post/{postId}",httpMethod = HttpMethod.DELETE)
	public Post removePost(
			@Named("postId") Long postId
			) {
		Post post = Post.get(postId);
		post.remove();
		return post;
		
	}
	
	@ApiMethod(name = "post.create", path="post",httpMethod = HttpMethod.POST)
	public Post createPost(Post post) {		
		post.save();
		return post;		
	}
	
	@ApiMethod(name = "post.update", path="post/{postId}",httpMethod = HttpMethod.POST)
	public Post updatePost(Post post) {		
		post.save();
		return post;		
	}	
}
