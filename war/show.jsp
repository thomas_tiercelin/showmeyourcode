<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.googlecode.objectify.ObjectifyService" %>
<%@ page import="java.util.List" %>
<%@ page import="com.showmeyourcode.Post" %>
<%!
	static {
		ObjectifyService.register(Post.class);
	}
%>
<!doctype html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" data-useragent="Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)" ng-app=>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>My VidBlog</title>

    
    <meta name="description" content="Documentation and reference library for ZURB Foundation. JavaScript, CSS, components, grid and more." />
    
    <meta name="author" content="ZURB, inc. ZURB network also includes zurb.com" />
    <meta name="copyright" content="ZURB, inc. Copyright (c) 2013" />

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/foundation/5.0.2/css/foundation.min.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.3/angular.min.js"></script>
  </head>
  <body>
    

<!-- Nav Bar -->

  <div class="row">
    <div class="large-12 columns">
      <div class="nav-bar right" ng-hide="true">
       <ul class="button-group">
         <li><a href="#" class="button">Link 1</a></li>
         <li><a href="#" class="button">Link 2</a></li>
         <li><a href="#" class="button">Link 3</a></li>
         <li><a href="#" class="button">Link 4</a></li>
        </ul>
      </div>
      <h1><a href="/">VidBlog</a> <small>This is my blog. It's awesome.</small></h1>
      <hr />
    </div>
  </div>

  <!-- End Nav -->


  <!-- Main Page Content and Sidebar -->

  <div class="row">

    <!-- Main Blog Content -->
    <div class="large-12 columns" role="content">


	<%
  Post post = Post.getForServletRequest(request);
	%>



      <article>

        <h3><%=post.getTitle()%></h3>
        <h6>Written on <%=post.getCreatedAt()%>.</h6>

        <div class="row">
          <div class="large-6 columns">
            <h4>
            	<%=post.getDescription()%>
            </h4>
          </div>
          <div class="large-6 columns">
            <iframe width="100%" height="240" src="//www.youtube.com/embed/<%=post.getYoutubeId()%>" frameborder="0" allowfullscreen></iframe>
          </div>
        </div>

        <p style="margin-top:1em"><%=post.getContent()%></p>

      </article>

      <hr />





    </div>

    <!-- End Main Content -->



    <!-- End Sidebar -->
  </div>

  <!-- End Main Content and Sidebar -->


  <!-- Footer -->

  <footer class="row">
    <div class="large-12 columns">
      <div class="row">
        <div class="large-6 columns">
          <p>&copy; Copyright no one at all. Go to town.</p>
        </div>
        <div class="large-6 columns">
          <ul class="inline-list right"  ng-hide="true">
            <li><a href="#">Link 1</a></li>
            <li><a href="#">Link 2</a></li>
            <li><a href="#">Link 3</a></li>
            <li><a href="#">Link 4</a></li>
          </ul>
        </div>
      </div>
    </div>
  </footer>
    <script src="../assets/js/jquery.js"></script>
    <script src="../assets/js/templates/foundation.js"></script>
    <script>
      $(document).foundation();

      var doc = document.documentElement;
      doc.setAttribute('data-useragent', navigator.userAgent);
    </script>
  </body>
</html>
