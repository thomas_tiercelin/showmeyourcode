<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.googlecode.objectify.ObjectifyService" %>
<%@ page import="com.showmeyourcode.Post" %>
<%!
	static {
		ObjectifyService.register(Post.class);
	}
%>
<!doctype html>
<html lang="en" ng-app>
<head>
  <meta charset="utf-8">
  <title>My Super Bootstrap</title>

  <link rel="stylesheet" href="https://cdn.jsdelivr.net/foundation/5.0.2/css/foundation.min.css"/>

  <style>
  </style>

  <!--  ANGULAR INCLUSION -->
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.3/angular.min.js"></script>

  <script>
  	function init() {
  		console.log("client loaded");
		//var ROOT = 'https://your_app_id.appspot.com/_ah/api';
		var ROOT = 'http://localhost:8888/_ah/api';
		gapi.client.load('smyc', 'v1', function() {
		  console.log("api loaded");

		  var scope = angular.element(window.document.getElementById("main")).scope();
		  scope.apiLoaded();

		}, ROOT);  		
  	}
  </script>  
  <script src="https://apis.google.com/js/client.js?onload=init"></script>  
</head>
<body ng-controller="MainCtrl" id="main">

	<div class="row">

		<div class="large-6 large-centered columns">

	<h1>Admin</h1>
	<hr/>	

	<div ng-controller="PostsCtrl" id="posts">


		<form ng-init="post = {}" ng-controller="PostCtrl">
			<label>Title</label>
			<input type="text" ng-model="post.title"/>

			<br/>
			<label>Description</label>
			<input type="text" ng-model="post.description"/>			

			<br/>
			<label>Path</label>
			<input type="text" ng-model="post.path"/>		

			<br/>
			<label>YouTube Id</label>
			<input type="text" ng-model="post.youtubeId"/>						

			<br/>
			<label>Content</label>
			<textarea ng-model="post.content"></textarea>

			<br/>

			<button ng-click="reset()" class="secondary">Cancel</button>
			<button ng-click="create(post)">Create</button>			
		</form>
		<hr/>


		<div ng-repeat="post in posts" ng-controller="PostCtrl">
			<strong>{{post.title}}</strong>
			<br/>
			<a target="_blank" href="{{post.path}}">{{post.path}}</a>
			<br/>
			{{post.description}}
			<button ng-click="remove(post)" class="alert">Remove</button>
			<button ng-click="edit()" ng-hide="editForm">Edit</button>			

			<form ng-show="editForm">
				<h2>Edit</h2>
				<div>
					<label>Title</label>
					<input type="text" ng-model="post.title"/>

					<br/>
					<label>Description</label>
					<input type="text" ng-model="post.description"/>			

					<br/>
					<label>Path</label>
					<input type="text" ng-model="post.path"/>		

					<br/>
					<label>YouTube Id</label>
					<input type="text" ng-model="post.youtubeId"/>						

					<br/>
					<label>Content</label>
					<textarea ng-model="post.content"></textarea>					
				</div>

				<button ng-click="cancelEdit()" class="secondary">Cancel</button>
				<button ng-click="update()">Update</button>
			</form>

			<hr/>
		</div>
	</div>

		</div>


	</div>




  <script>

  // CONTROLLER DEFINITION PART

  function MainCtrl($scope) {
  	$scope.apiLoaded = function() {
  		console.log("launch");

  		var scope = angular.element(window.document.getElementById("posts")).scope();
  		scope.list();
  	}
  }
  function PostsCtrl($scope) {
  	$scope.posts = [];

  	$scope.remove = function(post) {
  		gapi.client.smyc.post.remove({postId: post.id}).execute(function(resp){
  			if(typeof resp.error == 'undefined') {
  				$scope.posts.splice($scope.posts.indexOf(post),1);
  				$scope.$apply($scope.posts);
  			}  			
  		});  		
  		
  	}  	

  	$scope.create = function(post) {
  		gapi.client.smyc.post.create(post).execute(function(resp){
  			if(typeof resp.error == 'undefined') {
  				$scope.posts.unshift(resp.result);		
  				$scope.$apply($scope.posts);
  			}  			
  		});
  	}  	

  	$scope.list = function() {
  		gapi.client.smyc.post.list().execute(function(resp){
  			if(typeof resp.error == 'undefined') {
  				if(resp.items != null) {
  					$scope.posts = resp.result.items;
  					$scope.$apply($scope.posts);
  				}
  			}
  		});
  	}
  }
  function PostCtrl($scope) {

  	$scope.postCopy = angular.copy($scope.post);

  	$scope.edit = function() {
  		$scope.editForm = true;
  	}
  	$scope.cancelEdit = function() {
  		$scope.editForm = false;
  		$scope.postCopy = angular.copy($scope.post);
  	}  
  	$scope.reset = function() {
  		$scope.post = {};
  		$scope.postCopy = angular.copy($scope.post);
  	}    		
  	$scope.update = function() {
  		gapi.client.smyc.post.update($scope.post).execute(function(resp){
  			if(typeof resp.error == 'undefined') {
  				$scope.post = resp.result;
  				$scope.$apply($scope.post);
  			}  			
  		});  		
  	}
  }

  </script>


</body>
</html>
